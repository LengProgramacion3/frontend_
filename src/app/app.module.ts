import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListarComponent } from './Student/listar/listar.component';
import { AddComponent } from './Student/add/add.component';
import { EditComponent } from './Student/edit/edit.component';
import { ServiceService } from './Service/service.service'; 
import {HttpClientModule} from '@angular/common/http';
import { HomeComponent } from './Student/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ListarComponent,
    AddComponent,
    EditComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [ServiceService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
