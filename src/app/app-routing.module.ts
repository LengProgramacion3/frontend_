import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './Student/add/add.component';
import { EditComponent } from './Student/edit/edit.component';
import { ListarComponent } from './Student/listar/listar.component';
import { HomeComponent } from './Student/home/home.component';

const routes: Routes = [
  {path: 'list', component: ListarComponent},
  {path: 'add', component: AddComponent},
  {path: 'edit', component: EditComponent},
  {path: 'home', component: HomeComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
