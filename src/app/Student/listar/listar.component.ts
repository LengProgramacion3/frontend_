import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ServiceService} from '../../Service/service.service';
import Student from '../../Model/Student';
import { __importDefault } from 'tslib';


@Component({
  selector: 'app-listar',
  templateUrl: './listar.component.html',
  styleUrls: ['./listar.component.css']
})
export class ListarComponent implements OnInit {

  students!:Student[];
  constructor(private service:ServiceService,private router:Router) { }

  ngOnInit(): void {
    this.service.getStudent()
    .subscribe(data=>{
      this.students=data;
    })
  }

  Editar(student:Student):void{
    localStorage.setItem("id", student.id.toString());
    this.router.navigate(["edit"]);
  }

  Delete(student:Student){
    alert("¿Seguro de eliminar?");
    this.service.deleteStudent(student)
    .subscribe(data=>{
      this.students=this.students.filter(p=>p!==student);
      //this.router.navigate(["list"]);
    })
  }

}
