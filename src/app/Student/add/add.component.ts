import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Student from 'src/app/Model/Student';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  
  student:Student = new Student();
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit() {

  }

  Guardar(student:Student){
    this.service.createStudent(student)
    .subscribe(data=>{
      alert("Se ha guardado un registro.");
      this.router.navigate(["list"]);
    })
  }

}
