import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import Student from 'src/app/Model/Student';
import { ServiceService } from 'src/app/Service/service.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  student=new Student();
  constructor(private router:Router, private service:ServiceService) { }

  ngOnInit(): void {
    this.Editar();
  }

  Editar(){
    let id:any=localStorage.getItem("id");
    this.service.getStudentId(+id)
    .subscribe(data=>{
      this.student=data;
    })
  }
  Actualizar(student:Student){
    this.service.updateStudent(student)
    .subscribe(data=>{
      this.student = data;
      alert("Actualizado el registro");
      this.router.navigate(["list"]);
    })
  }
}
