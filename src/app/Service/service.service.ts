import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import  Student  from '../Model/Student';


@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(private http:HttpClient) { }

  Url="http://localhost:8080/student";

  getStudent(){
    return this.http.get<Student[]>(this.Url);
  }

  createStudent(student:Student){
    return this.http.post<Student>(this.Url, student);
  }

  getStudentId(id:number){
    return this.http.get<Student>(this.Url+"/"+id);
  }

  updateStudent(student:Student){
    return this.http.put<Student>(this.Url+"/"+student.id,student);
  }

  deleteStudent(student:Student){
    return this.http.delete<Student>(this.Url+"/"+student.id);
  }

}
