import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'test_01';

  constructor(private router:Router){}

  Listar(){
    this.router.navigate(["list"]);
  }

  Nuevo(){
    this.router.navigate(["add"]);
  }

  Home(){
    this.router.navigate(["home"]);
  }
}
